<?php

namespace ClubeDoIngressoSdk\Entity;

class Page
{

    //Display
    const DISPLAY_YES = 1;
    const DISPLAY_NO = 0;
    //Status
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    //Display Label
    const LABEL_STATUS = [
        self::DISPLAY_YES => 'Sim',
        self::DISPLAY_NO => 'Não',
    ];
    //Status Label
    const LABEL_DISPLAY = [
        self::DISPLAY_YES => 'Sim',
        self::DISPLAY_NO => 'Não',
    ];

}
