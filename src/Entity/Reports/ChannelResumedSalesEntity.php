<?php

namespace ClubeDoIngressoSdk\Entity\Reports;

class ChannelResumedSalesEntity
{

    //Sale Channels
    const CHANNEL_ONLINE = 'ONLINE';
    const CHANNEL_FEE_PDV = 'FEE_PDV';
    const CHANNEL_NO_FEE_PDV = 'NO_FEE_PDV';
    //Sale Channels Label
    const LABEL_CHANNEL = [
        self::CHANNEL_ONLINE => 'Online',
        self::CHANNEL_FEE_PDV => 'Ponto de Venda - Com Taxa',
        self::CHANNEL_NO_FEE_PDV => 'Ponto de Venda - Sem Taxa'
    ];

}
