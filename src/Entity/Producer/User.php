<?php

namespace ClubeDoIngressoSdk\Entity\Producer;

class User
{

    //Profile
    const PROFILE_ADMINISTRATOR = 1;
    const PROFILE_USER = 2;
    const LABEL_PROFILE = [
        self::PROFILE_ADMINISTRATOR => 'Administrador',
        self::PROFILE_USER => 'Usuário',
    ];
    //Active
    const ACTIVE_ACTIVE = 1;
    const ACTIVE_INACTIVE = 0;
    const LABEL_ACTIVE = [
        self::ACTIVE_ACTIVE => 'Ativo',
        self::ACTIVE_INACTIVE => 'Inativo',
    ];
    //Status
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const LABEL_STATUS = [
        self::STATUS_ACTIVE => 'Ativo',
        self::STATUS_INACTIVE => 'Inativo',
    ];

}
