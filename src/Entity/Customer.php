<?php

namespace ClubeDoIngressoSdk\Entity;

class Customer
{

    //Gender
    const GENDER_MALE = 'M';
    const GENDER_FEMALE = 'F';
    //Status
    const STATUS_BANNED = 0;
    const STATUS_NO_RESTRICTION = 1;
    const STATUS_CREDIT_RESTRICTION = 2;
    //Restriction
    const RESTRICTION_CREDIT_CARD = 'C';
    const RESTRICTION_BOLETO_BANCARIO = 'B';
    //Gender Label
    const LABEL_GENDER = [
        self::GENDER_MALE => 'Masculino',
        self::GENDER_FEMALE => 'Feminino',
    ];
    //Status Label
    const LABEL_STATUS = [
        self::STATUS_BANNED => 'Banido',
        self::STATUS_NO_RESTRICTION => 'Sem Restrição',
        self::STATUS_CREDIT_RESTRICTION => 'Restrição de crédito',
    ];
    //Restriction Label
    const LABEL_RESTRICTION = [
        self::RESTRICTION_CREDIT_CARD => 'Cartão de crédito',
        self::RESTRICTION_BOLETO_BANCARIO => 'Boleto bancário',
    ];

}
