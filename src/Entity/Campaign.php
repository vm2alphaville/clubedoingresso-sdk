<?php

namespace ClubeDoIngressoSdk\Entity;

/**
 * @Entity
 */
class Campaign
{

    //Active
    const ACTIVE_ACTIVE = 1;
    const ACTIVE_INACTIVE = 0;
    const LABEL_ACTIVE = [
        self::ACTIVE_ACTIVE => 'Ativo',
        self::ACTIVE_INACTIVE => 'Inativo',
    ];

}
