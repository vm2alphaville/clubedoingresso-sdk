<?php

namespace ClubeDoIngressoSdk\Entity;

class EvoucherTemplate
{

    // Tags
    const TAG_FULLNAME_ = 'T_NOME_COMPLETO';
    const TAG_EVENT_NAME = 'T_NOME_EVENTO';
    const TAG_EVENT_IMAGE = 'T_IMAGEM';
    const TAG_SECTOR = 'T_SETOR';
    const TAG_BATCH = 'T_LOTE';
    const TAG_TICKET_TYPE = 'T_INGRESSO_TIPO';
    const TAG_TICKET_GENDER = 'T_INGRESSO_SEXO';
    const TAG_TICKET_NAME = 'T_INGRESSO_NOME';
    const TAG_TICKET_DESCRIPTION = 'T_INGRESSO_DESCRICAO';
    const TAG_EVENT_DATE = 'T_DATA';
    const TAG_EVENT_LOCATION = 'T_LOCAL';
    const TAG_EVENT_OPENING = 'T_ABERTURA';
    const TAG_EVENT_START = 'T_INICIO';
    const TAG_EVENT_AGE_RATE = 'T_CLASSIFICACAO';
    const TAG_PRODUCER_NAME = 'T_PRODUTOR';
    const TAG_EVOUCHER_USER = 'T_USUARIO_EVOUCHER';
    const TAG_ORDER_NUMBER = 'T_NUM_PEDIDO';
    const TAG_PAYMENT_FORM = 'T_FORMA_PAGAMENTO';
    const TAG_QRCODE_IMAGE = 'T_IMAGEM_QRCODE';
    const TAG_QRCODE_NUMBER = 'T_CODIGO_QRCODE';
    const TAG_CARD_NUMBER = 'T_NUM_CARTAO';
    // Labels
    const LABEL_TAG = [
        self::TAG_FULLNAME_ => 'Nome completo de exibição da opção de ingresso',
        self::TAG_EVENT_NAME => 'Nome de exibição do evento',
        self::TAG_EVENT_IMAGE => 'Imagem do evento',
        self::TAG_SECTOR => 'Setor',
        self::TAG_BATCH => 'Lote',
        self::TAG_TICKET_TYPE => 'Tipo de ingresso (inteira, meia, etc..)',
        self::TAG_TICKET_GENDER => 'Sexo',
        self::TAG_TICKET_NAME => 'Nome do Ingresso',
        self::TAG_TICKET_DESCRIPTION => 'Descrição do ingresso',
        self::TAG_EVENT_DATE => 'Data',
        self::TAG_EVENT_LOCATION => 'Local',
        self::TAG_EVENT_OPENING => 'Horário de abertura',
        self::TAG_EVENT_START => 'Horário de início do evento',
        self::TAG_EVENT_AGE_RATE => 'Classificação etária',
        self::TAG_PRODUCER_NAME => 'Produtor',
        self::TAG_EVOUCHER_USER => 'Nome do usuário do voucher',
        self::TAG_ORDER_NUMBER => 'Número do pedido',
        self::TAG_PAYMENT_FORM => 'Forma de pagamento',
        self::TAG_QRCODE_IMAGE => 'Imagem QRcode',
        self::TAG_QRCODE_NUMBER => 'Código QRcode',
        self::TAG_CARD_NUMBER => 'Número do Cartão'
    ];

}
