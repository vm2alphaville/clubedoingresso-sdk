<?php

namespace ClubeDoIngressoSdk\Entity;

class AccountManager
{

    //Status
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    //Display Label
    const LABEL_STATUS = [
        self::STATUS_ACTIVE => 'Ativo',
        self::STATUS_INACTIVE => 'Inativo',
    ];

}
