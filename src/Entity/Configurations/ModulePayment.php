<?php

namespace ClubeDoIngressoSdk\Entity\Configurations;

class ModulePayment
{

    // MODULOS
    const CIELO = 1;
    const ITAU = 2;
    const BRADESCO = 3;
    const SANTANDER = 4;
    const BRASPAG = 5;
    // LABEL MODULOS
    const LABEL_MODULE = [
        self::CIELO => 'Cielo',
        self::ITAU => 'Itaú',
        self::BRADESCO => 'Bradesco',
        self::SANTANDER => 'Santander',
        self::BRASPAG => 'Braspag',
    ];
    // IDS
    const CIELO1 = 'cielo1';
    const CIELO2 = 'cielo2';
    const ITAU1 = 'itau';
    const BRADESCO1 = 'bradesco';
    const SANTANDER1 = 'santander';
    const BRASPAG1 = 'braspag';
    // labels
    const IDS_MODULE = [
        self::CIELO1 => self::CIELO,
        self::CIELO2 => self::CIELO,
        self::ITAU1 => self::ITAU,
        self::BRADESCO1 => self::BRADESCO,
        self::SANTANDER1 => self::SANTANDER,
        self::BRASPAG1 => self::BRASPAG,
    ];
    const MASTER = 'master';
    const VISA = 'visa';
    const DINERS = 'diners';
    const AMEX = 'amex';
    const ELO = 'elo';
    const AURA = 'aura';
    const JBC = 'jbc';
    const DISCOVER = 'discover';
    const BANCOSANTANDER = 'santander';
    const BANCOITAU = 'itau';
    // labels
    const LABEL_FLAG = [
        self::MASTER => 'Master',
        self::VISA => 'Visa',
        self::DINERS => 'Diners',
        self::AMEX => 'Amex',
        self::ELO => 'Elo',
        self::AURA => 'Aura',
        self::JBC => 'JBC',
        self::DISCOVER => 'Discover',
        self::BANCOSANTANDER => 'Banco Santander',
        self::BANCOITAU => 'Banco Itaú',
    ];
    const CREDITCARD = 'CreditCard';
    const DEBITCARD = 'DebitCard';
    const BOLETO = 'Boleto';
    const TRANSFERENCIA = 'Transferencia';
    // labels
    const LABEL_METODO = [
        self::CREDITCARD => 'Cartão de Crédito',
        self::DEBITCARD => 'Cartão de Débito',
        self::BOLETO => 'Boleto',
        self::TRANSFERENCIA => 'Transferência',
    ];
    const ACTIVE_ACTIVE = 1;
    const ACTIVE_INACTIVE = 0;
    const LABEL_ACTIVE = [
        self::ACTIVE_ACTIVE => 'Ativo',
        self::ACTIVE_INACTIVE => 'Inativo',
    ];

}
