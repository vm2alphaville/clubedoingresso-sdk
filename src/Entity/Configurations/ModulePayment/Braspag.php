<?php

namespace ClubeDoIngressoSdk\Entity\Configurations\ModulePayment;

class Braspag
{

    const TEST = 'teste';
    const PRODUCTION = 'producao';
    const LABEL_ENVIRONMENT = [
        self::TEST => 'Teste',
        self::PRODUCTION => 'Produção',
    ];
    // Provedor Crédito
    const SIMULADO = 'Simulado';
    const CIELO = 'Cielo';
    const CIELO30 = 'Cielo30';
    const REDECARD = 'Redecard';
    const REDE = 'Rede';
    const REDESITEF = 'RedeSitef';
    const CIELOSITEF = 'CieloSitef';
    const SANTANDERSITEF = 'SantanderSitef';
    const BANORTE = 'Banorte';
    const GETNET = 'Getnet';
    const FIRSTDATA = 'FirstData';
    const GLOBALPAYMENTS = 'GlobalPayments';
    const DMCARD = 'DMCard';
    const LABEL_PROVIDER_CREDIT = [
        self::SIMULADO => 'Simulado',
        self::CIELO => 'Cielo',
        self::CIELO30 => 'Cielo30',
        self::REDECARD => 'Redecard',
        self::REDE => 'Rede',
        self::REDESITEF => 'RedeSitef',
        self::CIELOSITEF => 'CieloSitef',
        self::SANTANDERSITEF => 'SantanderSitef',
        self::BANORTE => 'Banorte',
        self::GETNET => 'Getnet',
        self::FIRSTDATA => 'FirstData',
        self::GLOBALPAYMENTS => 'GlobalPayments',
        self::DMCARD => 'DMCard',
    ];
    // Provedor Débito
    const DCIELO = 'Cielo';
    const DGETNET = 'Getnet';
    const DFIRSTDATA = 'FirstData';
    const DGLOBALPAYMENTS = 'GlobalPayments';
    const LABEL_PROVIDER_DEBIT = [
        self::DCIELO => 'Cielo',
        self::DGETNET => 'Getnet',
        self::DFIRSTDATA => 'FirstData',
        self::DGLOBALPAYMENTS => 'GlobalPayments',
    ];

}
