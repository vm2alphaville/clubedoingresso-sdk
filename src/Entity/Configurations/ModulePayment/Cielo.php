<?php

namespace ClubeDoIngressoSdk\Entity\Configurations\ModulePayment;

class Cielo
{

    const TEST = 'teste';
    const PRODUCTION = 'producao';
    const LABEL_ENVIRONMENT = [
        self::TEST => 'Teste',
        self::PRODUCTION => 'Procução',
    ];

}
