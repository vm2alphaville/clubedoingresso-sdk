<?php

namespace ClubeDoIngressoSdk\Entity\Configurations\ModulePayment;

class Santander
{

    // Ambiente
    const TEST = 202;
    const PRODUCTION = 27;
    const LABEL_PKCE = [
        self::TEST => 'Teste',
        self::PRODUCTION => 'Produção',
    ];
    // Moeda
    const CDMO_REAL = 9;
    const LABEL_CDMO = [
        self::CDMO_REAL => 'Real',
    ];
    // Baixa devolução
    const CDBD_BD = 1;
    const CDBD_NBND = 2;
    const LABEL_CDBD = [
        self::CDBD_BD => 'Baixar/ Devolver',
        self::CDBD_NBND => 'Não Baixar/ Não Devolver',
    ];
    //Código de Protesto
    const CDPR_PDC = 1;
    const CDPR_PDU = 2;
    const CDPR_NP = 3;
    const LABEL_CDPR = [
        self::CDPR_PDC => 'Protestar Dias Corridos',
        self::CDPR_PDU => 'Protestar Dias Úteis',
        self::CDPR_NP => 'Não Protestar',
    ];
    //Código de Juros
    const CDJR_VD = 1;
    const CDJR_TM = 2;
    const CDJR_IS = 3;
    const LABEL_CDJR = [
        self::CDJR_VD => 'Valor por Dia',
        self::CDJR_TM => 'Taxa Mensal',
        self::CDJR_IS => 'Isento',
    ];
    //Código da Espécie
    const CDES_DM = '02';
    const CDES_DS = '04';
    const CDES_LC = '07';
    const CDES_LC2 = '30';
    const CDES_NP = '12';
    const CDES_NR = '13';
    const CDES_RC = '17';
    const CDES_AP = '20';
    const CDES_CH = '97';
    const CDES_ND = '98';
    const CDES_OU = '99';
    const LABEL_CDES = [
        self::CDES_DM => 'DM - DUPLICATA MERCANTIL',
        self::CDES_DS => 'DS - DUPLICATA DE SERVICO',
        self::CDES_LC => 'LC - LETRA DE CÂMBIO (SOMENTE PARA BANCO 353)',
        self::CDES_LC2 => 'LC - LETRA DE CÂMBIO (SOMENTE PARA BANCO 008)',
        self::CDES_NP => 'NP - NOTA PROMISSORIA',
        self::CDES_NR => 'NR - NOTA PROMISSORIA RURAL',
        self::CDES_RC => 'RC - RECIBO',
        self::CDES_AP => 'AP – APOLICE DE SEGURO',
        self::CDES_CH => 'CH – CHEQUE',
        self::CDES_ND => 'ND - NOTA PROMISSORIA DIRETA',
        self::CDES_OU => 'Outro',
    ];
    //Código do Desconto
    const CDDE_VF = 1;
    const CDDE_PA = 2;
    const CDDE_CD = 3;
    const LABEL_CDDE = [
        self::CDDE_VF => 'Valor Fixo Até a Data Informada',
        self::CDDE_PA => 'Percentual Até a Data Informada',
        self::CDDE_CD => 'Cancelamento de Desconto',
    ];
    //Código da Forma de Cadastramento
    const CDFC_CR = 1;
    const CDFC_CS = 2;
    const LABEL_CDFC = [
        self::CDFC_CR => 'Com Cadastramento (Cobrança Registrada)',
        self::CDFC_CS => 'Sem Cadastramento (Cobrança sem Registro)',
    ];
    //Código da carteira
    const CDCA_OUT = 1;
    const CDCA_CSR = 2;
    const LABEL_CDCA = [
        self::CDCA_OUT => 'Outro',
        self::CDCA_CSR => 'CSR – (Cobrança não registrada)',
    ];

}
