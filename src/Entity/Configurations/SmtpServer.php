<?php

namespace ClubeDoIngressoSdk\Entity\Configurations;

class SmtpServer
{

    // Active
    const ACTIVE_INACTIVE = 0;
    const ACTIVE_ACTIVE = 1;
    // Encryption
    const SSL_ENCRYPTION = 'ssl';
    const TLS_ENCRYPTION = 'tls';

}
