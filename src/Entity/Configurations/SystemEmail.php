<?php

namespace ClubeDoIngressoSdk\Entity\Configurations;

class SystemEmail
{

    const ACTIVE_INACTIVE = 0;
    const ACTIVE_ACTIVE = 1;
    const EMAIL_COPY_INACTIVE = 0;
    const EMAIL_COPY_ACTIVE = 1;

}
