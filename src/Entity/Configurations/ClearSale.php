<?php

namespace ClubeDoIngressoSdk\Entity\Configurations;

class ClearSale
{

    const ACTIVE_ACTIVE = 1;
    const ACTIVE_INACTIVE = 0;
    const LABEL_ACTIVE = [
        self::ACTIVE_ACTIVE => 'Ativo',
        self::ACTIVE_INACTIVE => 'Inativo',
    ];
    const TEST = 'teste';
    const PRODUCTION = 'producao';
    const LABEL_ENVIRONMENT = [
        self::TEST => 'Teste',
        self::PRODUCTION => 'Procução',
    ];

}
