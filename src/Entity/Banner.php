<?php

namespace ClubeDoIngressoSdk\Entity;

/**
 * @Entity
 */
class Banner
{

    //Status
    const STATUS_VIEWING = 1;
    const STATUS_QUEUE = 2;
    const STATUS_INACTIVE = 3;
    const LABEL_STATUS = [
        self::STATUS_VIEWING => 'Em exibição',
        self::STATUS_QUEUE => 'Em breve',
        self::STATUS_INACTIVE => 'Inativo',
    ];

}
