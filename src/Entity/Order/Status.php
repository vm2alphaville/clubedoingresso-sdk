<?php

namespace ClubeDoIngressoSdk\Entity\Order;

class Status
{

//Status
    const STATUS_WAITING = 0;
    const STATUS_PAIDOUT = 1;
    const STATUS_CANCELED = 2;
    const STATUS_REFUNDED = 3;
    const STATUS_NOTAUTHORIZED = 4;
    //Status Label
    const LABEL_STATUS = [
        self::STATUS_WAITING => 'Aguardando Pagamento',
        self::STATUS_PAIDOUT => 'Pago',
        self::STATUS_CANCELED => 'Cancelado',
        self::STATUS_REFUNDED => 'Cancelado e Reembolsado',
        self::STATUS_NOTAUTHORIZED => 'Não Autorizado',
    ];

}
