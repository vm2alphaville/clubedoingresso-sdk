<?php

namespace ClubeDoIngressoSdk\Entity\Order;

class Item
{

    //Gender
    const GENDER_MALE = 'M';
    const GENDER_FEMALE = 'F';
    //Type
    const TYPE_ENTIRE = 1;
    const TYPE_HALF = 2;
    //Gender Label
    const LABEL_GENDER = [
        self::GENDER_MALE => 'Masculino',
        self::GENDER_FEMALE => 'Feminino',
    ];
    // Type Label
    const LABEL_TYPE = [
        self::TYPE_ENTIRE => 'Inteira',
        self::TYPE_HALF => 'Meia',
    ];

}
