<?php

namespace ClubeDoIngressoSdk\Entity\Order;

class Ticket
{

    //Gender
    const GENDER_MALE = 'M';
    const GENDER_FEMALE = 'F';
    //Type
    const TYPE_ENTIRE = 1;
    const TYPE_HALF = 2;
    //Status
    const STATUS_NOTVALIDATED = 0;
    const STATUS_VALIDATED = 1;
    //Gender Label
    const LABEL_GENDER = [
        self::GENDER_MALE => 'Masculino',
        self::GENDER_FEMALE => 'Feminino',
    ];
    // Type Label
    const LABEL_TYPE = [
        self::TYPE_ENTIRE => 'Inteira',
        self::TYPE_HALF => 'Meia',
    ];
    // Status Label
    const LABEL_STATUS = [
        self::STATUS_NOTVALIDATED => 'Pendente de Validação',
        self::STATUS_VALIDATED => 'Validado',
    ];

}
