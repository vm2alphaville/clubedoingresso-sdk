<?php

namespace ClubeDoIngressoSdk\Entity\Pdv;

class User
{

    //Status
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    //Profile
    const PROFILE_ADMIN = 1;
    const PROFILE_USER = 3;
    //Status Label
    const LABEL_STATUS = [
        self::STATUS_ACTIVE => 'Ativo',
        self::STATUS_INACTIVE => 'Inativo',
    ];
    //Profile Label
    const LABEL_PROFILE = [
        self::PROFILE_ADMIN => 'Administrador',
        self::PROFILE_USER => 'Usuário',
    ];

}
