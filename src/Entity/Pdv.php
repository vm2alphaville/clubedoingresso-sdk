<?php

namespace ClubeDoIngressoSdk\Entity;

class Pdv
{

    //Payment Methods
    const PAYMENT_CREDIT_CARD = 'CC';
    const PAYMENT_DEBIT_CARD = 'CD';
    const PAYMENT_MONEY = 'D';
    //Status
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    //Payment Label
    const LABEL_PAYMENT = [
        self::PAYMENT_CREDIT_CARD => 'Cartão de Crédito',
        self::PAYMENT_DEBIT_CARD => 'Cartão de Débito',
        self::PAYMENT_MONEY => 'Dinheiro',
    ];
    //Status Label
    const LABEL_STATUS = [
        self::STATUS_ACTIVE => 'Ativo',
        self::STATUS_INACTIVE => 'Inativo',
    ];

}
