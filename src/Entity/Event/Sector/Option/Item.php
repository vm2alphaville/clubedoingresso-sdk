<?php

namespace ClubeDoIngressoSdk\Entity\Event\Sector\Option;

class Item
{

    //Active
    const ACTIVE_ACTIVE = 1;
    const ACTIVE_INACTIVE = 0;
    //Active Label
    const LABEL_ACTIVE = [
        self::ACTIVE_ACTIVE => 'Ativo',
        self::ACTIVE_INACTIVE => 'Inativo',
    ];
    //Gender
    const GENDER_MALE = 'M';
    const GENDER_FEMALE = 'F';
    const LABEL_GENDER = [
        self::GENDER_MALE => 'Masculino',
        self::GENDER_FEMALE => 'Feminino',
    ];

}
