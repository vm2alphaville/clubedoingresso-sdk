<?php

namespace ClubeDoIngressoSdk\Entity\Event\Sector;

class Row
{

    //Type
    const TYPE_ROW_SEAT = 1;
    const TYPE_ROW_TABLE = 2;
    //Status
    const STATUS_UNAVAILABLE = 0;
    const STATUS_AVAILABLE = 1;
    const STATUS_SOLDOUT = 2;
    //Label Type
    const LABEL_ROW_TYPE = [
        self::TYPE_ROW_SEAT => 'Fileira de Cadeiras',
        self::TYPE_ROW_TABLE => 'Fileira de Mesas',
    ];
    //Label Status
    const LABEL_STATUS = [
        self::STATUS_UNAVAILABLE => 'Indisponível',
        self::STATUS_AVAILABLE => 'Disponível',
        self::STATUS_SOLDOUT => 'Esgotada',
    ];

}
