<?php

namespace ClubeDoIngressoSdk\Entity\Event\Sector;

class Batch
{

    //Active
    const ACTIVE_ACTIVE = 1;
    const ACTIVE_INACTIVE = 0;
    //Status
    const STATUS_UNAVAILABLE = 0;
    const STATUS_AVAILABLE = 1;
    const STATUS_SOON = 2;
    const STATUS_SOLDOUT = 3;
    const STATUS_CANCELED = 4;
    const STATUS_EXPIRED = 5;
    //Label
    const LABEL_STATUS = [
        self::STATUS_UNAVAILABLE => 'Indisponível',
        self::STATUS_AVAILABLE => 'Disponível',
        self::STATUS_SOON => 'Em breve',
        self::STATUS_SOLDOUT => 'Esgotado',
        self::STATUS_CANCELED => 'Cancelado',
        self::STATUS_EXPIRED => 'Expirado',
    ];
    //
    const LABEL_ACTIVE = [
        self::ACTIVE_INACTIVE => 'Inativo',
        self::ACTIVE_ACTIVE => 'Ativo',
    ];

}
