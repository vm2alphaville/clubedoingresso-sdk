<?php

namespace ClubeDoIngressoSdk\Entity\Event\Sector;

class Option
{

    //Active
    const ACTIVE_ACTIVE = 1;
    const ACTIVE_INACTIVE = 0;
    //Active Label
    const LABEL_ACTIVE = [
        self::ACTIVE_ACTIVE => 'Ativo',
        self::ACTIVE_INACTIVE => 'Inativo',
    ];
    //Type
    const TYPE_ENTIRE = 1;
    const TYPE_HALF = 2;
    const LABEL_TYPE = [
        self::TYPE_ENTIRE => 'Inteira',
        self::TYPE_HALF => 'Meia',
    ];

}
