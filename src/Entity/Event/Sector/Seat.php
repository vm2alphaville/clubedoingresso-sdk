<?php

namespace ClubeDoIngressoSdk\Entity\Event\Sector;

class Seat
{

    //Status
    const STATUS_UNAVAILABLE = 0;
    const STATUS_AVAILABLE = 1;
    const STATUS_RESERVED = 2;
    const STATUS_SOLD = 3;
    //Label Status
    const LABEL_STATUS = [
        self::STATUS_UNAVAILABLE => 'Indisponível',
        self::STATUS_AVAILABLE => 'Disponível',
        self::STATUS_RESERVED => 'Reservado',
        self::STATUS_SOLD => 'Vendida',
    ];

}
