<?php

namespace ClubeDoIngressoSdk\Entity\Event\Sector;

class Table
{

    //Flag
    const FLAG_NOT_NUMBERED_SEAT = 0;
    const FLAG_NUMBERED_SEAT = 1;
    const FLAG_CLOSED = 2;
    //Status
    const STATUS_UNAVAILABLE = 0;
    const STATUS_AVAILABLE = 1;
    const STATUS_RESERVED = 2;
    const STATUS_SOLDOUT = 3;
    //Label Flag
    const LABEL_FLAGS = [
        self::FLAG_NOT_NUMBERED_SEAT => 'Sem lugar marcado',
        self::FLAG_NUMBERED_SEAT => 'Lugar marcado',
        self::FLAG_CLOSED => 'Fechada',
    ];
    //Label Status
    const LABEL_STATUS = [
        self::STATUS_UNAVAILABLE => 'Indisponível',
        self::STATUS_AVAILABLE => 'Disponível',
        self::STATUS_RESERVED => 'Reservado',
        self::STATUS_SOLDOUT => 'Esgotada',
    ];

}
