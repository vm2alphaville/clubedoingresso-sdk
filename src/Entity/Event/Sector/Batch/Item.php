<?php

namespace Ticket360Sdk\Entity\Event\Sector\Batch;

class Item
{

    //Active
    const ACTIVE_ACTIVE = 1;
    const ACTIVE_INACTIVE = 0;
    //Gender
    const GENDER_MALE = 'M';
    const GENDER_FEMALE = 'F';
    //Limit
    const LIMIT_PERCENTAGE = 1;
    const LIMIT_INTEGER_NUMBER = 2;
    //Status
    const STATUS_UNAVAILABLE = 0;
    const STATUS_AVAILABLE = 1;
    const STATUS_SOON = 2;
    const STATUS_SOLDOUT = 3;
    const STATUS_CANCELED = 4;
    const STATUS_EXPIRED = 5;
    //Label
    const LABEL_STATUS = [
        self::STATUS_UNAVAILABLE => 'Indisponível',
        self::STATUS_AVAILABLE => 'Disponível',
        self::STATUS_SOON => 'Em breve',
        self::STATUS_SOLDOUT => 'Esgotado',
        self::STATUS_CANCELED => 'Cancelado',
        self::STATUS_EXPIRED => 'Expirado',
    ];
    //
    const LABEL_ACTIVE = [
        self::ACTIVE_INACTIVE => 'Inativo',
        self::ACTIVE_ACTIVE => 'Ativo',
    ];
    const LABEL_GENDER = [
        self::GENDER_MALE => 'Masculino',
        self::GENDER_FEMALE => 'Feminino',
    ];
    const LIMIT_TYPE = [
        self::LIMIT_PERCENTAGE => 'Porcentagem',
        self::LIMIT_INTEGER_NUMBER => 'Valor Numérico Inteiro',
    ];

}
