<?php

namespace ClubeDoIngressoSdk\Entity\Event;

class Sector
{

    //Type
    const TYPE_SIMPLE = 1;
    const TYPE_ROW = 2;
    const TYPE_TABLE = 3;
    const TYPE_ROW_TABLE = 4;
    //Channels
    const CHANNEL_WEB = 'W';
    const CHANNEL_PDV = 'P';
    //Flag
    const FLAG_NOT_NUMBERED_SEAT = 0;
    const FLAG_NUMBERED_SEAT = 1;
    const FLAG_CLOSED = 2;
    //Status
    const STATUS_UNAVAILABLE = 0;
    const STATUS_AVAILABLE = 1;
    const STATUS_SOON = 2;
    const STATUS_SOLDOUT = 3;
    const STATUS_CANCELED = 4;
    const STATUS_EXPIRED = 5;
    //Label
    const LABEL_STATUS = [
        self::STATUS_UNAVAILABLE => 'Indisponível',
        self::STATUS_AVAILABLE => 'Disponível',
        self::STATUS_SOON => 'Em breve',
        self::STATUS_SOLDOUT => 'Esgotado',
        self::STATUS_CANCELED => 'Cancelado',
        self::STATUS_EXPIRED => 'Expirado',
    ];
    //Label Type
    const LABEL_TYPE = [
        self::TYPE_SIMPLE => 'Setor Simples',
        self::TYPE_ROW => 'Fileira de Cadeiras',
        self::TYPE_TABLE => 'Mesas Livres',
        self::TYPE_ROW_TABLE => 'Fileira de Mesas',
    ];
    //Label Channels
    const LABEL_CHANNEL = [
        self::CHANNEL_WEB => 'Web',
        self::CHANNEL_PDV => 'PDV',
    ];
    //Label Flag
    const LABEL_FLAGS = [
        self::FLAG_NOT_NUMBERED_SEAT => 'Sem lugar marcado',
        self::FLAG_NUMBERED_SEAT => 'Lugar marcado',
        self::FLAG_CLOSED => 'Fechado',
    ];

}
