<?php

namespace ClubeDoIngressoSdk\Exception;

use Exception;

class UnprocessableEntity extends Exception
{

    /**
     * @var array
     */
    protected $validationMessages = [];

    /**
     * @param string $message
     * @param array $validationMessages
     * @param int $code
     * @param Exception $previous
     */
    public function __construct($message, array $validationMessages, $code = 0, Exception $previous = null)
    {
        $this->setValidationMessages($validationMessages);
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return array
     */
    public function getValidationMessages()
    {
        return $this->validationMessages;
    }

    /**
     * @param array $validationMessages
     * @return \ClubeDoIngressoSdk\Exception\UnprocessableEntity
     */
    public function setValidationMessages(array $validationMessages)
    {
        $this->validationMessages = $validationMessages;
        return $this;
    }

}
