<?php

namespace ClubeDoIngressoSdk\Service\Campaign;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use ClubeDoIngressoSdk\TraitCacheable;
use ClubeDoIngressoSdk\Request as ApiRequest;
use Zend\Http\Request as HttpRequest;
use Zend\Http\Response as HttpResponse;
use Zend\Json\Decoder as JsonDecoder;
use Zend\Json\Encoder as JsonEncoder;
use ClubeDoIngressoSdk\Exception;
use Zend\Paginator\Paginator;
use ClubeDoIngressoSdk\Paginator\Adapter\Embedded as EmbeddedAdapter;

/**
 * @Service
 */
class Coupon extends ApiRequest implements ServiceLocatorAwareInterface
{

    use TraitCacheable;

    const ENDPOINT_URL = '/campaign/coupon';
    const ENDPOINT_URL_IMPORT = '/campaign/coupon/import';
    const ENDPOINT_URL_VERIFY = '/campaign/coupon/verify';

    /**
     * @param array $data
     * @return stdClass Coupon
     * @throws Exception\UnprocessableEntity
     * @throws \Exception
     */
    public function create(array $data)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL);
        $request->setMethod(HttpRequest::METHOD_POST);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(JsonEncoder::encode($data));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_201) { //Created
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_422) { //Unprocessable Entity
            throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param int $id
     * @param array $data
     * @return stdClass Updated Coupon
     * @throws Exception\UnprocessableEntity
     * @throws Exception\EntityNotFound
     * @throws \Exception
     */
    public function update(int $id, array $data)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL . "/{$id}");
        $request->setMethod(HttpRequest::METHOD_PATCH);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(JsonEncoder::encode($data));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_422) { //Unprocessable Entity
            throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_404) { //Not Found
            throw new Exception\EntityNotFound($responseContent->detail, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param string $filter
     * @param string $sort
     * @param int $page
     * @param int $pageSize
     * @return Paginator
     * @throws \Exception
     */
    public function fetchAll(string $filter = null, string $sort = null, int $page = 1, int $pageSize = 30)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL);
        $request->setMethod(HttpRequest::METHOD_GET);
        $request->getQuery()->set('page', $page);
        $request->getQuery()->set('page_size', $pageSize);
        $request->getQuery()->set('filter', urlencode($filter));
        $request->getQuery()->set('sort', urlencode($sort));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            $paginator = new Paginator(new EmbeddedAdapter($responseContent->_embedded->coupons, $responseContent->total_items));
            $paginator->setCurrentPageNumber($responseContent->page)
                ->setItemCountPerPage($responseContent->page_size);

            return $paginator;
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param int $id
     * @return stdClass Campaign entity
     * @throws Exception\EntityNotFound
     * @throws \Exception
     */
    public function fetch(int $id)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL . "/{$id}");
        $request->setMethod(HttpRequest::METHOD_GET);

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_404) { //Not Found
            throw new Exception\EntityNotFound($responseContent->detail, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param int $campaign
     * @param string $file
     * @return array
     * @throws Exception\UnprocessableEntity
     * @throws \Exception
     */
    public function import(int $campaign, string $file)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL_IMPORT);
        $request->setMethod(HttpRequest::METHOD_POST);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(JsonEncoder::encode(['campaign' => $campaign, 'file' => $file]));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_406) { //Not Acceptable
            throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception\UnprocessableEntity
     * @throws \Exception
     */
    public function verify(array $data)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL_VERIFY);
        $request->setMethod(HttpRequest::METHOD_POST);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(JsonEncoder::encode($data));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_422) {
            if (isset($responseContent->errno) && $responseContent->errno == 'IC1') {
                throw new Exception\Campaign\Coupon\InvalidCoupon($responseContent->detail, $responseContent->status);
            } else if (isset($responseContent->validation_messages)) {
                throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
            }
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

}
