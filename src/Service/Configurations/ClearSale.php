<?php

namespace ClubeDoIngressoSdk\Service\Configurations;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use ClubeDoIngressoSdk\TraitCacheable;
use ClubeDoIngressoSdk\Request as ApiRequest;
use Zend\Http\Request as HttpRequest;
use Zend\Http\Response as HttpResponse;
use Zend\Json\Decoder as JsonDecoder;
use Zend\Json\Encoder as JsonEncoder;
use ClubeDoIngressoSdk\Exception;

class ClearSale extends ApiRequest implements ServiceLocatorAwareInterface
{

    use TraitCacheable;

    // Endpoints
    const ENDPOINT_URL_READ = '/clearsale/read';
    const ENDPOINT_URL_WRITE = '/clearsale/write';

    /**
     * @param int $id
     * @return stdClass AnalyticsConfig entity
     * @throws Exception\EntityNotFound
     * @throws \Exception
     */
    public function fetch()
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL_READ);
        $request->setMethod(HttpRequest::METHOD_GET);

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_404) { //Not Found
            throw new Exception\EntityNotFound($responseContent->detail, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param array $data
     * @return stdClass Updated clearsale
     * @throws Exception\UnprocessableEntity
     * @throws Exception\EntityNotFound
     * @throws \Exception
     */
    public function update(array $data)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL_WRITE);
        $request->setMethod(HttpRequest::METHOD_POST);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(JsonEncoder::encode($data));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_422) { //Unprocessable Entity
            throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_404) { //Not Found
            throw new Exception\EntityNotFound($responseContent->detail, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

}
