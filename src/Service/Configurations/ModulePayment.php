<?php

namespace ClubeDoIngressoSdk\Service\Configurations;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use ClubeDoIngressoSdk\TraitCacheable;
use ClubeDoIngressoSdk\Request as ApiRequest;
use Zend\Http\Request as HttpRequest;
use Zend\Http\Response as HttpResponse;
use Zend\Json\Decoder as JsonDecoder;
use Zend\Json\Encoder as JsonEncoder;
use ClubeDoIngressoSdk\Exception;

class ModulePayment extends ApiRequest implements ServiceLocatorAwareInterface
{

    use TraitCacheable;

    // Endpoints
    const ENDPOINT_URL = '/modulepayment';
    const ENDPOINT_URL_READ = '/modulepayment/read';
    const ENDPOINT_URL_WRITE = '/modulepayment/write';
    const ENDPOINT_URL_ACTIVATE = '/modulepayment/activate';
    const ENDPOINT_URL_DEACTIVATE = '/modulepayment/deactivate';
    const ENDPOINT_URL_AVAILABLES = '/modulepayment/payment/available';

    /**
     * @return array
     * @throws \Exception
     */
    public function fetchAll()
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL);
        $request->setMethod(HttpRequest::METHOD_GET);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //ok
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_422) { //Unprocessable Entity
            throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param array $items
     * @param int $customer
     * @param float $ordeValue
     * @return array
     * @throws Exception\UnprocessableEntity
     * @throws \Exception
     */
    public function fetchAllAvailables(array $items, int $customer, float $ordeValue)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL_AVAILABLES);
        $request->setMethod(HttpRequest::METHOD_POST);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(
            JsonEncoder::encode([
                'items' => $items,
                'customer' => $customer,
                'orderValue' => $ordeValue
            ])
        );
        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //ok
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_422) { //Unprocessable Entity
            throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param string $id
     * @return stdClass Updated modulepayment
     * @throws \Exception
     */
    public function fetch(string $id)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL_READ);
        $request->setMethod(HttpRequest::METHOD_POST);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(
            JsonEncoder::encode([
                'id' => $id
            ])
        );
        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //ok
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_422) { //Unprocessable Entity
            throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param array $data
     * @return stdClass Updated modulepayment
     * @throws Exception\UnprocessableEntity
     * @throws Exception\EntityNotFound
     * @throws \Exception
     */
    public function update(array $data)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL_WRITE);
        $request->setMethod(HttpRequest::METHOD_POST);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(JsonEncoder::encode($data));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_422) { //Unprocessable Entity
            throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_404) { //Not Found
            throw new Exception\EntityNotFound($responseContent->detail, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param int $id
     * @return stdClass ModulePayment entity
     * @throws Exception\UnprocessableEntity
     * @throws \Exception
     */
    public function activate(string $id)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL_ACTIVATE);
        $request->setMethod(HttpRequest::METHOD_POST);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(JsonEncoder::encode(['id' => $id]));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_406) { //Not Acceptable
            throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param int $id
     * @return stdClass ModulePayment entity
     * @throws Exception\UnprocessableEntity
     * @throws Exception\Producer\User\MustHaveAdmin
     * @throws \Exception
     */
    public function deactivate(string $id)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL_DEACTIVATE);
        $request->setMethod(HttpRequest::METHOD_POST);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(JsonEncoder::encode(['id' => $id]));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_406) { //Not Acceptable
            throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_422) { //Unprocessable Entity
            throw new Exception\Producer\User\MustHaveAdmin($responseContent->detail, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

}
