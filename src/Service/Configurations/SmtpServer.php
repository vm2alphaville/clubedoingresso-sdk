<?php

namespace ClubeDoIngressoSdk\Service\Configurations;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use ClubeDoIngressoSdk\TraitCacheable;
use ClubeDoIngressoSdk\Request as ApiRequest;
use Zend\Http\Request as HttpRequest;
use Zend\Http\Response as HttpResponse;
use Zend\Json\Decoder as JsonDecoder;
use Zend\Json\Encoder as JsonEncoder;
use ClubeDoIngressoSdk\Exception;
use Zend\Paginator\Paginator;
use ClubeDoIngressoSdk\Paginator\Adapter\Embedded as EmbeddedAdapter;

class SmtpServer extends ApiRequest implements ServiceLocatorAwareInterface
{

    use TraitCacheable;

    const ENDPOINT_URL = '/config/smtp-server';

    /**
     * @param array $data
     * @return stdClass Created ServidorSmtpConfig
     * @throws Exception\UnprocessableEntity
     * @throws \Exception
     */
    public function create(array $data)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL);
        $request->setMethod(HttpRequest::METHOD_POST);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(JsonEncoder::encode($data));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_201) { //Created
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_422) { //Unprocessable Entity
            throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param int $id
     * @param array $data
     * @return stdClass Updated ServidorSmtpConfig
     * @throws Exception\UnprocessableEntity
     * @throws Exception\EntityNotFound
     * @throws \Exception
     */
    public function update(int $id, array $data)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL . "/{$id}");
        $request->setMethod(HttpRequest::METHOD_PATCH);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(JsonEncoder::encode($data));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_422) { //Unprocessable Entity
            throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_404) { //Not Found
            throw new Exception\EntityNotFound($responseContent->detail, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param int $id
     * @return stdClass ServidorSmtpConfig entity
     * @throws Exception\EntityNotFound
     * @throws \Exception
     */
    public function fetch(int $id)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL . "/{$id}");
        $request->setMethod(HttpRequest::METHOD_GET);

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_404) { //Not Found
            throw new Exception\EntityNotFound($responseContent->detail, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param string $filter
     * @param string $sort
     * @param int $page
     * @param int $pageSize
     * @return Paginator
     * @throws \Exception
     */
    public function fetchAll(string $filter = null, string $sort = null, int $page = 1, int $pageSize = 30)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL);
        $request->setMethod(HttpRequest::METHOD_GET);
        $request->getQuery()->set('page', $page);
        $request->getQuery()->set('page_size', $pageSize);
        $request->getQuery()->set('filter', urlencode($filter));
        $request->getQuery()->set('sort', urlencode($sort));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            $paginator = new Paginator(new EmbeddedAdapter($responseContent->_embedded->smtp_configs, $responseContent->total_items));
            $paginator->setCurrentPageNumber($responseContent->page)
                ->setItemCountPerPage($responseContent->page_size);

            return $paginator;
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param string $email
     * @param string $credential
     * @return stdClass Customer entity
     * @throws Exception\Customer\Authenticate\CredentialInvalid
     * @throws Exception\Customer\Authenticate\Failure
     */
    public function authenticate(string $email, string $credential)
    {
        $postParams = [
            'email' => $email,
            'credential' => $credential,
        ];

        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::AUTHENTICATE_ENDPOINT_URL);
        $request->setMethod(HttpRequest::METHOD_POST);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(JsonEncoder::encode($postParams));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_406) { //Not Acceptable
            throw new Exception\Customer\Authenticate\CredentialInvalid($responseContent->detail, $response->getStatusCode());
        } else {
            throw new Exception\Customer\Authenticate\Failure($responseContent->detail, $response->getStatusCode());
        }
    }

}
