<?php

namespace ClubeDoIngressoSdk\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use ClubeDoIngressoSdk\TraitCacheable;
use ClubeDoIngressoSdk\Request as ApiRequest;
use Zend\Http\Request as HttpRequest;
use Zend\Http\Response as HttpResponse;
use Zend\Json\Decoder as JsonDecoder;
use Zend\Json\Encoder as JsonEncoder;
use ClubeDoIngressoSdk\Exception;

class TicketValidation extends ApiRequest implements ServiceLocatorAwareInterface
{

    use TraitCacheable;

    const ENDPOINT_URL_LIST_SECTORS = '/validationGetSectors';
    const ENDPOINT_URL_VALIDATE = '/validateTicket';

    /**
     * @param array $data
     * @return array
     * @throws Exception\UnprocessableEntity
     * @throws Exception\EntityNotFound
     * @throws \Exception
     */
    public function getSectors(array $data)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL_LIST_SECTORS);
        $request->setMethod(HttpRequest::METHOD_POST);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(JsonEncoder::encode($data));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_201 || $response->getStatusCode() == HttpResponse::STATUS_CODE_200) {
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_422) { //Unprocessable Entity
            throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_404) { //Not Found
            throw new Exception\EntityNotFound($responseContent->detail, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception\UnprocessableEntity
     * @throws Exception\EntityNotFound
     * @throws \Exception
     */
    public function validateTicket(array $data)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL_VALIDATE);
        $request->setMethod(HttpRequest::METHOD_POST);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(JsonEncoder::encode($data));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_201 || $response->getStatusCode() == HttpResponse::STATUS_CODE_200) {
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_422) { //Unprocessable Entity
            throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_404) { //Not Found
            throw new Exception\EntityNotFound($responseContent->detail, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

}
