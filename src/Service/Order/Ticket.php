<?php

namespace ClubeDoIngressoSdk\Service\Order;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use ClubeDoIngressoSdk\TraitCacheable;
use ClubeDoIngressoSdk\Request as ApiRequest;
use Zend\Http\Request as HttpRequest;
use Zend\Http\Response as HttpResponse;
use Zend\Json\Decoder as JsonDecoder;
use Zend\Json\Encoder as JsonEncoder;
use ClubeDoIngressoSdk\Exception;
use Zend\Paginator\Paginator;
use ClubeDoIngressoSdk\Paginator\Adapter\Embedded as EmbeddedAdapter;

class Ticket extends ApiRequest implements ServiceLocatorAwareInterface
{

    use TraitCacheable;

    const ENDPOINT_URL = '/ticket';
    const ENDPOINT_URL_DOWNLOAD = '/ticket/download';
    const ENDPOINT_URL_SEND = '/ticket/send';

    /**
     * @param int $id
     * @return stdClass Sector entity
     * @throws Exception\EntityNotFound
     * @throws \Exception
     */
    public function fetch(int $id)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL . "/{$id}");
        $request->setMethod(HttpRequest::METHOD_GET);

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_404) { //Not Found
            throw new Exception\EntityNotFound($responseContent->detail, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param string $filter
     * @param string $sort
     * @param int $page
     * @param int $pageSize
     * @return Paginator
     * @throws \Exception
     */
    public function fetchAll(string $filter = null, string $sort = null, int $page = 1, int $pageSize = 30)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL);
        $request->setMethod(HttpRequest::METHOD_GET);
        $request->getQuery()->set('page', $page);
        $request->getQuery()->set('page_size', $pageSize);
        $request->getQuery()->set('filter', urlencode($filter));
        $request->getQuery()->set('sort', urlencode($sort));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            $paginator = new Paginator(new EmbeddedAdapter($responseContent->_embedded->tickets, $responseContent->total_items));
            $paginator->setCurrentPageNumber($responseContent->page)
                ->setItemCountPerPage($responseContent->page_size);

            return $paginator;
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param int $id
     * @return array
     * @throws Exception\UnprocessableEntity
     * @throws \Exception
     */
    public function download(int $id)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL_DOWNLOAD);
        $request->setMethod(HttpRequest::METHOD_POST);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(JsonEncoder::encode(['ticket' => $id]));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) {
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_422) { //Unprocessable Entity
            throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param int $id
     * @return array
     * @throws Exception\UnprocessableEntity
     * @throws \Exception
     */
    public function send(int $id)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL_SEND);
        $request->setMethod(HttpRequest::METHOD_POST);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(JsonEncoder::encode(['order' => $id]));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) {
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_422) { //Unprocessable Entity
            throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

}
