<?php

namespace ClubeDoIngressoSdk\Service\Event\Sector;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use ClubeDoIngressoSdk\TraitCacheable;
use ClubeDoIngressoSdk\Request as ApiRequest;
use Zend\Http\Request as HttpRequest;
use Zend\Http\Response as HttpResponse;
use Zend\Json\Decoder as JsonDecoder;
use Zend\Json\Encoder as JsonEncoder;
use ClubeDoIngressoSdk\Exception;
use Zend\Paginator\Paginator;
use ClubeDoIngressoSdk\Paginator\Adapter\Embedded as EmbeddedAdapter;

class Seat extends ApiRequest implements ServiceLocatorAwareInterface
{

    use TraitCacheable;

    const ENDPOINT_URL = '/event/sector/seat';

    /**
     * @param array $data
     * @return stdClass Created Seat
     * @throws Exception\UnprocessableEntity
     * @throws \Exception
     */
    public function create(array $data)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL);
        $request->setMethod(HttpRequest::METHOD_POST);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(JsonEncoder::encode($data));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_201) { //Created
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_422) { //Unprocessable Entity
            throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param int $id
     * @param array $data
     * @return stdClass Updated Seat
     * @throws Exception\UnprocessableEntity
     * @throws Exception\EntityNotFound
     * @throws \Exception
     */
    public function update(int $id, array $data)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL . "/{$id}");
        $request->setMethod(HttpRequest::METHOD_PATCH);
        $request->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $request->setContent(JsonEncoder::encode($data));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_422) { //Unprocessable Entity
            throw new Exception\UnprocessableEntity($responseContent->detail, (array) $responseContent->validation_messages, $responseContent->status);
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_404) { //Not Found
            throw new Exception\EntityNotFound($responseContent->detail, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param int $id
     * @return stdClass Seat entity
     * @throws Exception\EntityNotFound
     * @throws \Exception
     */
    public function fetch(int $id)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL . "/{$id}");
        $request->setMethod(HttpRequest::METHOD_GET);

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            return $responseContent;
        } elseif ($response->getStatusCode() == HttpResponse::STATUS_CODE_404) { //Not Found
            throw new Exception\EntityNotFound($responseContent->detail, $responseContent->status);
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

    /**
     * @param string $filter
     * @param string $sort
     * @param int $page
     * @param int $pageSize
     * @return Paginator
     * @throws \Exception
     */
    public function fetchAll(string $filter = null, string $sort = null, int $page = 1, int $pageSize = 30)
    {
        $request = new HttpRequest;
        $request->setUri($this->getOptions()->getEndpoint() . self::ENDPOINT_URL);
        $request->setMethod(HttpRequest::METHOD_GET);
        $request->getQuery()->set('page', $page);
        $request->getQuery()->set('page_size', $pageSize);
        $request->getQuery()->set('filter', urlencode($filter));
        $request->getQuery()->set('sort', urlencode($sort));

        $response = $this->request($request);
        $responseContent = JsonDecoder::decode($response->getContent());

        if ($response->getStatusCode() == HttpResponse::STATUS_CODE_200) { //Ok
            $paginator = new Paginator(new EmbeddedAdapter($responseContent->_embedded->seats, $responseContent->total_items));
            $paginator->setCurrentPageNumber($responseContent->page)
                ->setItemCountPerPage($responseContent->page_size);

            return $paginator;
        } else {
            throw new \Exception($responseContent->detail, $responseContent->status);
        }
    }

}
