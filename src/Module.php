<?php

namespace ClubeDoIngressoSdk;

use Zend\ModuleManager\Feature\ServiceProviderInterface;

class Module implements ServiceProviderInterface
{

    /**
     * @var string
     */
    const CONFIG_KEY = 'clubedoingresso-sdk';

    public function getServiceConfig()
    {
        return [
            self::CONFIG_KEY => [
                'endpoint' => 'https://api.clubedoingresso.com.br',
                'auth_url' => 'https://api.clubedoingresso.com.br/oauth2',
                'client_id' => '',
                'client_secret' => '',
            ],
            'factories' => [
                'ClubeDoIngressoSdk\Options' => 'ClubeDoIngressoSdk\OptionsFactory',
            ],
            'invokables' => [
                'ClubeDoIngressoSdk\CustomerService' => 'ClubeDoIngressoSdk\Service\Customer',
                'ClubeDoIngressoSdk\PageService' => 'ClubeDoIngressoSdk\Service\Page',
                'ClubeDoIngressoSdk\AccountManagerService' => 'ClubeDoIngressoSdk\Service\AccountManager',
                'ClubeDoIngressoSdk\Configurations\GoogleAnalyticsService' => 'ClubeDoIngressoSdk\Service\Configurations\GoogleAnalytics',
                'ClubeDoIngressoSdk\Configurations\OrderService' => 'ClubeDoIngressoSdk\Service\Configurations\Order',
                'ClubeDoIngressoSdk\Configurations\SmtpServerService' => 'ClubeDoIngressoSdk\Service\Configurations\SmtpServer',
                'ClubeDoIngressoSdk\Configurations\SystemEmailService' => 'ClubeDoIngressoSdk\Service\Configurations\SystemEmail',
                'ClubeDoIngressoSdk\Configurations\ModulePaymentService' => 'ClubeDoIngressoSdk\Service\Configurations\ModulePayment',
                'ClubeDoIngressoSdk\Configurations\ModulePayment\PaymentService' => 'ClubeDoIngressoSdk\Service\Configurations\ModulePayment\Payment',
                'ClubeDoIngressoSdk\Configurations\ClearSaleService' => 'ClubeDoIngressoSdk\Service\Configurations\ClearSale',
                'ClubeDoIngressoSdk\LocationService' => 'ClubeDoIngressoSdk\Service\Location',
                'ClubeDoIngressoSdk\PdvService' => 'ClubeDoIngressoSdk\Service\Pdv',
                'ClubeDoIngressoSdk\Pdv\UserService' => 'ClubeDoIngressoSdk\Service\Pdv\User',
                'ClubeDoIngressoSdk\ProducerService' => 'ClubeDoIngressoSdk\Service\Producer',
                'ClubeDoIngressoSdk\Producer\UserService' => 'ClubeDoIngressoSdk\Service\Producer\User',
                'ClubeDoIngressoSdk\CategoryService' => 'ClubeDoIngressoSdk\Service\Category',
                'ClubeDoIngressoSdk\BannerService' => 'ClubeDoIngressoSdk\Service\Banner',
                'ClubeDoIngressoSdk\EventService' => 'ClubeDoIngressoSdk\Service\Event',
                'ClubeDoIngressoSdk\Event\CategoryService' => 'ClubeDoIngressoSdk\Service\Event\Category',
                'ClubeDoIngressoSdk\Event\ProducerService' => 'ClubeDoIngressoSdk\Service\Event\Producer',
                'ClubeDoIngressoSdk\Event\FinancialTransactionService' => 'ClubeDoIngressoSdk\Service\Event\FinancialTransaction',
                'ClubeDoIngressoSdk\EvoucherTemplateEditorService' => 'ClubeDoIngressoSdk\Service\EvoucherTemplateEditor',
                'ClubeDoIngressoSdk\Event\PaymentModuleService' => 'ClubeDoIngressoSdk\Service\Event\PaymentModule',
                'ClubeDoIngressoSdk\Event\SectorService' => 'ClubeDoIngressoSdk\Service\Event\Sector',
                'ClubeDoIngressoSdk\Event\Sector\RowService' => 'ClubeDoIngressoSdk\Service\Event\Sector\Row',
                'ClubeDoIngressoSdk\Event\Sector\TableService' => 'ClubeDoIngressoSdk\Service\Event\Sector\Table',
                'ClubeDoIngressoSdk\Event\Sector\SeatService' => 'ClubeDoIngressoSdk\Service\Event\Sector\Seat',
                'ClubeDoIngressoSdk\Event\Sector\Seat\TypeService' => 'ClubeDoIngressoSdk\Service\Event\Sector\Seat\Type',
                'ClubeDoIngressoSdk\Event\Sector\OptionService' => 'ClubeDoIngressoSdk\Service\Event\Sector\Option',
                'ClubeDoIngressoSdk\Event\Sector\Option\ItemService' => 'ClubeDoIngressoSdk\Service\Event\Sector\Option\Item',
                'ClubeDoIngressoSdk\Event\Sector\BatchService' => 'ClubeDoIngressoSdk\Service\Event\Sector\Batch',
                'ClubeDoIngressoSdk\Event\Sector\Batch\ItemService' => 'ClubeDoIngressoSdk\Service\Event\Sector\Batch\Item',
                'ClubeDoIngressoSdk\Event\Sector\PdvService' => 'ClubeDoIngressoSdk\Service\Event\Sector\Pdv',
                'ClubeDoIngressoSdk\CampaignService' => 'ClubeDoIngressoSdk\Service\Campaign',
                'ClubeDoIngressoSdk\Campaign\EventService' => 'ClubeDoIngressoSdk\Service\Campaign\Event',
                'ClubeDoIngressoSdk\Campaign\CouponService' => 'ClubeDoIngressoSdk\Service\Campaign\Coupon',
                'ClubeDoIngressoSdk\OrderService' => 'ClubeDoIngressoSdk\Service\Order',
                'ClubeDoIngressoSdk\Order\ItemService' => 'ClubeDoIngressoSdk\Service\Order\Item',
                'ClubeDoIngressoSdk\Order\StatusService' => 'ClubeDoIngressoSdk\Service\Order\Status',
                'ClubeDoIngressoSdk\Order\TicketService' => 'ClubeDoIngressoSdk\Service\Order\Ticket',
                'ClubeDoIngressoSdk\TicketValidationService' => 'ClubeDoIngressoSdk\Service\TicketValidation',
                'ClubeDoIngressoSdk\GalleryService' => 'ClubeDoIngressoSdk\Service\Gallery',
                'ClubeDoIngressoSdk\InformService' => 'ClubeDoIngressoSdk\Service\Inform',
                'ClubeDoIngressoSdk\Reports\EventSales\ResumedSalesService' => 'ClubeDoIngressoSdk\Service\Reports\EventSales\ResumedSales',
                'ClubeDoIngressoSdk\Reports\EventSales\ChannelResumedSalesService' => 'ClubeDoIngressoSdk\Service\Reports\EventSales\ChannelResumedSales',
                'ClubeDoIngressoSdk\Reports\EventSales\ChannelDetailedSalesService' => 'ClubeDoIngressoSdk\Service\Reports\EventSales\ChannelDetailedSales',
                'ClubeDoIngressoSdk\Reports\EventSales\DebtBalanceService' => 'ClubeDoIngressoSdk\Service\Reports\EventSales\DebtBalance',
                'ClubeDoIngressoSdk\Reports\EventSales\EventSalesFileExtractionService' => 'ClubeDoIngressoSdk\Service\Reports\EventSales\EventSalesFileExtraction',
                'ClubeDoIngressoSdk\ExtractService' => 'ClubeDoIngressoSdk\Service\Extract',
                'ClubeDoIngressoSdk\Reports\CampaignSalesService' => 'ClubeDoIngressoSdk\Service\Reports\CampaignSales',
                'ClubeDoIngressoSdk\Reports\CampaignSalesFileExtractionService' => 'ClubeDoIngressoSdk\Service\Reports\CampaignSalesFileExtraction',
                'ClubeDoIngressoSdk\Reports\PdvSalesService' => 'ClubeDoIngressoSdk\Service\Reports\PdvSales',
                'ClubeDoIngressoSdk\Reports\PdvSalesFileExtractionService' => 'ClubeDoIngressoSdk\Service\Reports\PdvSalesFileExtraction',
                'ClubeDoIngressoSdk\Reports\ConsolidatedSalesService' => 'ClubeDoIngressoSdk\Service\Reports\ConsolidatedSales',
                'ClubeDoIngressoSdk\Reports\ConsolidatedSalesFileExtractionService' => 'ClubeDoIngressoSdk\Service\Reports\ConsolidatedSalesFileExtraction',
                'ClubeDoIngressoSdk\Reports\PendingPaymentBoletosService' => 'ClubeDoIngressoSdk\Service\Reports\PendingPaymentBoletos',
                'ClubeDoIngressoSdk\Reports\PendingPaymentBoletosFileExtractionService' => 'ClubeDoIngressoSdk\Service\Reports\PendingPaymentBoletosFileExtraction',
                'ClubeDoIngressoSdk\Reports\ClubeDebtBalanceService' => 'ClubeDoIngressoSdk\Service\Reports\ClubeDebtBalance',
                'ClubeDoIngressoSdk\Reports\ClubeDebtBalanceFileExtractionService' => 'ClubeDoIngressoSdk\Service\Reports\ClubeDebtBalanceFileExtraction',
                'ClubeDoIngressoSdk\Reports\ValidationsService' => 'ClubeDoIngressoSdk\Service\Reports\Validations',
                'ClubeDoIngressoSdk\Reports\ValidationsFileExtractionService' => 'ClubeDoIngressoSdk\Service\Reports\ValidationsFileExtraction',
                'ClubeDoIngressoSdk\Reports\TaxesIncomeService' => 'ClubeDoIngressoSdk\Service\Reports\TaxesIncome',
                'ClubeDoIngressoSdk\Reports\TaxesIncomeFileExtractionService' => 'ClubeDoIngressoSdk\Service\Reports\TaxesIncomeFileExtraction',
            ],
        ];
    }

}
