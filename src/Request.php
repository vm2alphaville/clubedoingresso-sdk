<?php

namespace ClubeDoIngressoSdk;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container as SessionContainer;
use Zend\Http\Request as HttpRequest;
use Zend\Http\Client as HttpClient;

class Request implements ServiceLocatorAwareInterface
{

    protected $serviceLocator;

    /**
     * @var \Zend\Session\Container
     */
    protected $session;

    public function __construct()
    {
        $this->session = new SessionContainer('ClubeDoIngressoSdk_ApiToken');
    }

    /**
     * @return \ClubeDoIngressoSdk\Options
     */
    public function getOptions()
    {
        return $this->getServiceLocator()->get('ClubeDoIngressoSdk\Options');
    }

    protected function requestToken()
    {
        $options = $this->getOptions();

        $request = new HttpRequest;
        $request->setUri($options->getAuthUrl());
        $request->setMethod(HttpRequest::METHOD_POST);
        $request->getPost()
            ->set('grant_type', 'client_credentials')
            ->set('client_id', $options->getClientId())
            ->set('client_secret', $options->getClientSecret());

        $httpClient = new HttpClient;
        $httpClient->setOptions([
            'adapter' => \Zend\Http\Client\Adapter\Curl::class,
            'timeout' => 60,
        ]);
        $response = $httpClient->dispatch($request);

        $token = \Zend\Json\Decoder::decode($response->getContent());
        $this->session->offsetSet('token', $token);
        return $token;
    }

    public function request(HttpRequest $request)
    {
        $token = $this->session->offsetGet('token') ?: $this->requestToken();

        $request->getHeaders()->addHeaders([
            'Authorization' => "{$token->token_type} {$token->access_token}",
            'Accept' => 'application/hal+json; charset=UTF-8',
        ]);

        $httpClient = new HttpClient;
        $httpClient->setOptions([
            'adapter' => \Zend\Http\Client\Adapter\Curl::class,
            'timeout' => 60,
        ]);
        $response = $httpClient->dispatch($request);

        if (in_array($response->getStatusCode(), array(\Zend\Http\Response::STATUS_CODE_401, \Zend\Http\Response::STATUS_CODE_403))) {
            $this->session->offsetUnset('token');
            return $this->request($request);
        }

        return $response;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

}
