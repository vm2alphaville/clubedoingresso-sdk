<?php

namespace ClubeDoIngressoSdk;

use Zend\Stdlib\AbstractOptions;

class Options extends AbstractOptions
{

    /**
     * @var string
     */
    protected $endpoint;

    /**
     * @var string
     */
    protected $authUrl;

    /**
     * @var string
     */
    protected $clientId;

    /**
     * @var string
     */
    protected $clientSecret;

    public function getEndpoint()
    {
        return $this->endpoint;
    }

    public function getAuthUrl()
    {
        return $this->authUrl;
    }

    public function getClientId()
    {
        return $this->clientId;
    }

    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    public function setEndpoint(string $endpoint)
    {
        $this->endpoint = $endpoint;
        return $this;
    }

    public function setAuthUrl(string $authUrl)
    {
        $this->authUrl = $authUrl;
        return $this;
    }

    public function setClientId(string $clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    public function setClientSecret(string $clientSecret)
    {
        $this->clientSecret = $clientSecret;
        return $this;
    }

}
