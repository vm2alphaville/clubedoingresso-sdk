<?php

namespace ClubeDoIngressoSdk\Paginator\Adapter;

use Zend\Paginator\Adapter\AdapterInterface;

class Embedded implements AdapterInterface
{

    /**
     * @var array
     */
    protected $items = [];

    /**
     * @var int
     */
    protected $totalItems = 0;

    /**
     * @param array $items
     * @param int $totalItems
     */
    public function __construct(array $items, int $totalItems)
    {
        $this->items = $items;
        $this->totalItems = $totalItems;
    }

    public function count($mode = 'COUNT_NORMAL')
    {
        return $this->totalItems;
    }

    public function getItems($offset, $itemCountPerPage)
    {
        return $this->items;
    }

}
