<?php

namespace ClubeDoIngressoSdk;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class OptionsFactory implements FactoryInterface
{

    /**
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @return \ClubeDoIngressoSdk\Options
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');
        if (!isset($config[Module::CONFIG_KEY])) {
            throw new \InvalidArgumentException('ClubeDoIngressoSdk settings are not set!');
        }

        $options = new Options();
        $options->setFromArray($config[Module::CONFIG_KEY]);

        return $options;
    }

}
