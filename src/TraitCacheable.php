<?php

namespace ClubeDoIngressoSdk;

use Zend\Cache\Storage\StorageInterface;

trait TraitCacheable
{

    private $METHOD_SUFIX_FOR_CACHE = 'FromCache';

    /**
     * @var \Zend\Cache\Storage\StorageInterface
     */
    protected $storage;

    /**
     * @param \Zend\Cache\Storage\StorageInterface $storage
     * @return \Application\Stdlib\Cacheable
     */
    public function setCacheStorage(StorageInterface $storage)
    {
        $this->storage = $storage;
        return $this;
    }

    /**
     * @return \Zend\Cache\Storage\StorageInterface
     */
    public function getCacheStorage()
    {
        return $this->storage;
    }

    public function buildCacheHash($method, array $arguments = array())
    {
        $class = get_class($this);
        $method = str_replace($this->METHOD_SUFIX_FOR_CACHE, '', $method);

        $params = $arguments;
        foreach ($arguments as $key => $value) {
            if (is_object($value) && method_exists($value, '__toString')) {
                $params[$key] = $value->__toString();
            }
            $params[$key] = (string) $params[$key];
        }
        $params = var_export($params, true);

        $hash = "{$class}_{$method}_{$params}";
        $hash = str_replace(array("\r\n", "\n", "\r", "\t", " "), '', $hash);

        return md5($hash);
    }

    public function __call($name, $arguments)
    {
        $contents = null;

        if (strstr($name, $this->METHOD_SUFIX_FOR_CACHE)) {
            if (!$this->getCacheStorage()) {
                throw new \Exception("You need to set the cache storage to invoke 'FromCache' methods!");
            }

            $hash = $this->buildCacheHash($name, $arguments);
            if ($this->getCacheStorage()->hasItem($hash)) {
                $contents = $this->getCacheStorage()->getItem($hash);
            } else {
                $method = str_replace($this->METHOD_SUFIX_FOR_CACHE, '', $name);
                $contents = call_user_func_array(array($this, $method), $arguments);
                $this->getCacheStorage()->setItem($hash, $contents);
            }

            return $contents;
        }
    }

}
